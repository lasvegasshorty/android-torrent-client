package com.github.axet.torrentclient.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.TreeListView;
import com.github.axet.androidlibrary.widgets.TreeRecyclerView;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.TorrentApplication;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import libtorrent.Libtorrent;

public class FilesFragment extends Fragment implements MainActivity.TorrentFragmentInterface {
    View v;
    TreeRecyclerView list;
    View toolbar;
    View download;

    Files adapter;
    TextView size;
    long t;

    public static class TorName {
        public String fullPath;
        public String path; // sort by value
        public String name; // display value
        public long size;

        public String toString() {
            return path;
        }
    }

    public static class TorFolder extends TorName {
        public TreeListView.TreeNode node;
        long t;

        public TorFolder(long t) {
            this.t = t;
        }

        public boolean getCheck() {
            boolean b = true;
            for (TreeListView.TreeNode n : node.nodes) {
                TorName m = (TorName) n.tag;
                TorFile k = (TorFile) m;
                if (!k.getCheck())
                    b = false;
            }
            return b;
        }

        public void setCheck(boolean b) {
            Libtorrent.torrentFilesCheckFilter(t, fullPath + "/*", b);
            for (TreeListView.TreeNode n : node.nodes) {
                TorName m = (TorName) n.tag;
                TorFile k = (TorFile) m;
                k.file.setCheck(b); // update java side runtime data
            }
        }
    }

    public static class TorFile extends TorName {
        public TorFolder parent;
        public long index;
        public libtorrent.File file;
        public long t;
        public TreeListView.TreeNode node;

        public TorFile(long t, long i) {
            this.t = t;
            index = i;
            update();
            size = file.getLength();
        }

        public void update() {
            file = Libtorrent.torrentFiles(t, index);
        }

        public void setCheck(boolean b) {
            Libtorrent.torrentFilesCheck(t, index, b);
            file.setCheck(b); // update java side runtime data
        }

        public boolean getCheck() {
            return file.getCheck(); // read from runtime data
        }
    }

    public static class SortFiles implements Comparator<TreeListView.TreeNode> {
        @Override
        public int compare(TreeListView.TreeNode file, TreeListView.TreeNode file2) {
            TorName f1 = (TorName) file.tag;
            TorName f2 = (TorName) file2.tag;
            return f1.path.compareTo(f2.path);
        }
    }

    public static class FileHolder extends TreeRecyclerView.TreeHolder {
        TextView percent;
        View folder;
        View fc;
        TextView folderName;
        TextView file;
        CheckBox fcheck;
        TextView fsize;
        ImageView expand;
        CheckBox check;
        TextView size;

        public FileHolder(View view) {
            super(view);
            percent = (TextView) view.findViewById(R.id.torrent_files_percent);
            folder = view.findViewById(R.id.torrent_files_folder);
            fc = view.findViewById(R.id.torrent_files_file);
            folderName = (TextView) view.findViewById(R.id.torrent_files_folder_name);
            file = (TextView) view.findViewById(R.id.torrent_files_name);
            fcheck = (CheckBox) view.findViewById(R.id.torrent_files_folder_check);
            fsize = (TextView) view.findViewById(R.id.torrent_files_folder_size);
            expand = (ImageView) view.findViewById(R.id.torrent_files_folder_expand);
            check = (CheckBox) view.findViewById(R.id.torrent_files_check);
            size = (TextView) view.findViewById(R.id.torrent_files_size);
        }
    }

    public static class Files extends TreeRecyclerView.TreeAdapter<FileHolder> {
        Context context;
        HashMap<String, TorFolder> folders = new HashMap<>();

        public Files(Context context) {
            this.context = context;
        }

        public long getTorrent() {
            return 0;
        }

        @Override
        public int getItemViewType(int position) {
            TreeListView.TreeNode n = getItem(position);
            return n.nodes.isEmpty() ? 0 : 1;
        }

        @Override
        public FileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View convertView = inflater.inflate(R.layout.torrent_files_item, parent, false);
            return new FileHolder(convertView);
        }

        @Override
        public void onBindViewHolder(final FileHolder h, int position) {
            TreeListView.TreeNode n = getItem(h.getAdapterPosition(this));
            TorName item = (TorName) n.tag;

            if (item instanceof TorFolder) {
                h.folder.setVisibility(View.VISIBLE);
                h.fc.setVisibility(View.GONE);
                final TorFolder f = (TorFolder) item;
                h.folderName.setText(f.name);

                h.fcheck.setChecked(f.getCheck());
                if (Build.VERSION.SDK_INT >= 11)
                    h.fcheck.jumpDrawablesToCurrentState();
                h.fcheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        f.setCheck(h.fcheck.isChecked());
                        updateTotal();
                        notifyDataSetChanged();
                    }
                });

                h.fsize.setText(TorrentApplication.formatSize(context, item.size));

                if (!n.expanded)
                    h.expand.setImageResource(R.drawable.ic_expand_more_black_24dp);
                else
                    h.expand.setImageResource(R.drawable.ic_expand_less_black_24dp);
            }

            if (item instanceof TorFile) {
                final long t = getTorrent();
                h.fc.setVisibility(View.VISIBLE);
                h.folder.setVisibility(View.GONE);

                final TorFile f = (TorFile) item;
                f.update();

                h.check.setChecked(f.getCheck());
                if (Build.VERSION.SDK_INT >= 11)
                    h.check.jumpDrawablesToCurrentState();
                h.check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Libtorrent.torrentFilesCheck(t, f.index, h.check.isChecked());
                    }
                });

                h.percent.setEnabled(false);
                if (f.file.getLength() > 0)
                    TorrentApplication.setTextNA(h.percent, (f.file.getBytesCompleted() * 100 / f.file.getLength()) + "%");
                else
                    TorrentApplication.setTextNA(h.percent, "100%");

                h.fc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Libtorrent.torrentFilesCheck(t, f.index, h.check.isChecked());
                        f.file.setCheck(h.check.isChecked()); // update java side runtime data
                        updateTotal();
                        notifyDataSetChanged();
                    }
                });

                h.file.setText(f.name);

                h.size.setText(TorrentApplication.formatSize(context, item.size));

                if (f.parent == null)
                    h.itemView.setBackgroundColor(0);
                else
                    h.itemView.setBackgroundColor(ThemeUtils.getThemeColor(context, R.attr.colorButtonNormal));
            }
        }

        public void update() {
            long t = getTorrent();
            long l = Libtorrent.torrentFilesCount(t);

            String torrentName = Libtorrent.torrentName(t);

            if (root.nodes.size() == 0 && l > 0) {
                root.nodes.clear();
                folders.clear();
                if (l == 1) {
                    TorFile f = new TorFile(t, 0);
                    f.name = TorrentApplication.DOTSLASH + f.file.getPath();
                    TreeListView.TreeNode n = new TreeListView.TreeNode(root, f);
                    f.node = n;
                    root.nodes.add(n);
                } else {
                    for (long i = 0; i < l; i++) {
                        TorFile f = new TorFile(t, i);

                        String p = f.file.getPath();
                        f.fullPath = p;
                        String fp = p.substring(torrentName.length() + 1);
                        f.path = fp;
                        File file = new File(fp);
                        String parent = file.getParent();
                        f.name = TorrentApplication.DOTSLASH + file.getName();

                        if (parent != null) {
                            TorFolder folder = folders.get(parent);
                            if (folder == null) {
                                folder = new TorFolder(t);
                                folder.fullPath = new File(p).getParent();
                                folder.path = parent;
                                folder.name = folder.path;
                                TreeListView.TreeNode n = new TreeListView.TreeNode(root, folder);
                                folder.node = n;
                                root.nodes.add(n);
                                folders.put(parent, folder);
                            }
                            folder.size += f.size;
                            TreeListView.TreeNode n = new TreeListView.TreeNode(folder.node, f);
                            f.node = n;
                            folder.node.nodes.add(n);
                            f.parent = folder;
                        }
                        if (f.parent == null) {
                            TreeListView.TreeNode n = new TreeListView.TreeNode(root, f);
                            f.node = n;
                            root.nodes.add(n);
                        }
                    }
                    for (TreeListView.TreeNode n : root.nodes) {
                        if (n.tag instanceof TorFolder) {
                            TorFolder m = (TorFolder) n.tag;
                            Collections.sort(m.node.nodes, new SortFiles());
                        }
                    }
                    Collections.sort(root.nodes, new SortFiles());
                }
            }

            load();
        }

        public void checkAll() {
            long t = getTorrent();
            Libtorrent.torrentFilesCheckAll(t, true);
            for (TreeListView.TreeNode n : root.nodes) {
                if (n.tag instanceof TorFolder) {
                    TorFolder f = (TorFolder) n.tag;
                    for (TreeListView.TreeNode k : f.node.nodes) {
                        TorFile m = (TorFile) k.tag;
                        m.file.setCheck(true); // update java side runtime data
                    }
                }
                if (n.tag instanceof TorFile) {
                    TorFile f = (TorFile) n.tag;
                    f.file.setCheck(true); // update java side runtime data
                }
            }
            updateTotal();
            notifyDataSetChanged();
        }

        public void checkNone() {
            long t = getTorrent();
            Libtorrent.torrentFilesCheckAll(t, false);
            for (TreeListView.TreeNode n : root.nodes) {
                if (n.tag instanceof TorFolder) {
                    TorFolder f = (TorFolder) n.tag;
                    for (TreeListView.TreeNode k : f.node.nodes) {
                        TorFile m = (TorFile) k.tag;
                        m.file.setCheck(false); // update java side runtime data
                    }
                }
                if (n.tag instanceof TorFile) {
                    TorFile f = (TorFile) n.tag;
                    f.file.setCheck(false); // update java side runtime data
                }
            }
            updateTotal();
            notifyDataSetChanged();
        }

        public void updateTotal() {
        }
    }

    void updateTotal() {
        String p = "";
        if (Libtorrent.metaTorrent(t)) {
            p = TorrentApplication.formatSize(getContext(), Libtorrent.torrentPendingBytesLength(t));
        }
        TorrentApplication.setTextNA(size, p);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.torrent_files, container, false);

        t = getArguments().getLong("torrent");

        download = v.findViewById(R.id.torrent_files_metadata);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Libtorrent.downloadMetadata(t)) {
                    ErrorDialog.Error(getActivity(), Libtorrent.error());
                    return;
                }
            }
        });

        list = (TreeRecyclerView) v.findViewById(R.id.list);

        toolbar = v.findViewById(R.id.torrent_files_toolbar);

        adapter = new Files(getContext()) {
            @Override
            public long getTorrent() {
                return getArguments().getLong("torrent");
            }

            @Override
            public void updateTotal() {
                FilesFragment.this.updateTotal();
            }
        };

        list.setAdapter(adapter);

        DividerItemDecoration div = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        list.addItemDecoration(div);

        size = (TextView) v.findViewById(R.id.torrent_files_size);

        View none = v.findViewById(R.id.torrent_files_none);
        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.checkNone();
            }
        });

        View all = v.findViewById(R.id.torrent_files_all);
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.checkAll();
            }
        });

        View delete = v.findViewById(R.id.torrent_files_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.delete_unselected);
                builder.setMessage(R.string.are_you_sure);
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ;
                    }
                });
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Libtorrent.torrentFileDeleteUnselected(t);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        update();

        return v;
    }

    @Override
    public void update() {
        long t = getArguments().getLong("torrent");

        download.setVisibility(Libtorrent.metaTorrent(t) ? View.GONE : View.VISIBLE);
        toolbar.setVisibility(Libtorrent.metaTorrent(t) ? View.VISIBLE : View.GONE);

        adapter.update();
        updateTotal();
    }

    @Override
    public void close() {
    }
}
