package com.github.axet.torrentclient.services;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.provider.DocumentFile;
import android.webkit.MimeTypeMap;

import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.services.StorageProvider;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.app.TorrentPlayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;

// <application>
//   <provider
//     android:name="com.github.axet.torrentclient.services.TorrentContentProvider"
//     android:authorities="com.github.axet.torrentclient"
//     android:exported="false"
//     android:grantUriPermissions="true">
//   </provider>
// </application>
//
// url example:
// content://com.github.axet.torrentclient/778811221de5b06a33807f4c80832ad93b58016e/image.rar/123.mp3
//
public class TorrentContentProvider extends StorageProvider {
    public static String TAG = TorrentContentProvider.class.getSimpleName();

    public static final int HASH_SIZE = 40;

    HashMap<TorrentPlayer, Long> players = new HashMap<>();
    Runnable refresh = new Runnable() {
        @Override
        public void run() {
            freePlayers();
        }
    };

    public static TorrentContentProvider getProvider() {
        return (TorrentContentProvider) infos.get(TorrentContentProvider.class);
    }

    public static String getTypeByPath(String filePath) {
        String ext = MimeTypeMap.getFileExtensionFromUrl(Uri.encode(filePath));
        return Storage.getTypeByName(ext);
    }

    public static Intent openFolderIntent(Context context, Storage.Torrent t) {
        Uri u = t.path;
        String s = u.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            File f = Storage.getFile(u);
            File n = new File(f, t.name());
            if (n.exists() && n.isDirectory())
                u = Uri.fromFile(n);
        } else if (Build.VERSION.SDK_INT >= 21 && s.equals(ContentResolver.SCHEME_CONTENT)) {
            Uri doc = Storage.getDocumentChild(context, u, t.name());
            DocumentFile file = Storage.getDocumentFile(context, doc);
            if (file.exists() && file.isDirectory())
                u = file.getUri();
        } else {
            throw new Storage.UnknownUri();
        }
        return openFolderIntent(context, u);
    }

    public static Intent openFolderIntent(Context context, Uri uri) {
        String s = uri.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            return StorageProvider.openFolderIntent(context, uri); // file:// only method
        } else if (s.equals(ContentResolver.SCHEME_CONTENT)) {
            return TorrentContentProvider.openFolderIntent23(context, uri);
        } else {
            throw new Storage.UnknownUri();
        }
    }

    public static boolean isFolderCallable(Context context, Intent intent) {
        return TorrentContentProvider.isFolderCallable(context, intent, TorrentContentProvider.getProvider().getAuthority());
    }

    public Uri getUriForFile(String hash, String file) {
        File f = new File(hash, file);
        String name = f.toString();
        return new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(getAuthority()).path(name).build();
    }

    TorrentPlayer find(String hash) {
        for (TorrentPlayer p : players.keySet()) {
            if (p.torrentHash.equals(hash))
                return p;
        }
        return null;
    }

    void freePlayers() {
        long now = System.currentTimeMillis();
        for (TorrentPlayer p : new HashSet<>(players.keySet())) {
            long l = players.get(p);
            if (l + TIMEOUT < now) {
                p.close();
                players.remove(p);
            }
        }
        if (players.size() == 0)
            return;
        handler.removeCallbacks(refresh);
        handler.postDelayed(refresh, TIMEOUT);
    }

    TorrentPlayer.PlayerFile getPlayerFile(Uri uri) {
        TorrentPlayer.PlayerFile file = null;
        TorrentApplication app = TorrentApplication.from(getContext());
        if (app.player != null)
            file = app.player.find(uri);
        if (file == null) {
            String hash = uri.getPathSegments().get(0);
            Storage storage = app.storage;
            TorrentPlayer player = find(hash);
            if (player == null)
                player = new TorrentPlayer(getContext(), storage, storage.find(hash).t);
            players.put(player, System.currentTimeMillis()); // refresh last access time
            file = player.find(uri);
            freePlayers();
        }
        return file;
    }

    @Override
    public boolean onCreate() {
        if (!super.onCreate())
            return false;
        deleteTmp();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder, CancellationSignal cancellationSignal) {
        if (isStorageUri(uri))
            return super.query(uri, projection, selection, selectionArgs, sortOrder, cancellationSignal);
        TorrentPlayer.PlayerFile f = getPlayerFile(uri);
        if (f == null)
            return null;
        if (projection == null)
            projection = FileProvider.COLUMNS;
        Object[] values = new Object[projection.length];
        int i = 0;
        for (String col : projection) {
            if (OpenableColumns.DISPLAY_NAME.equals(col)) {
                values[i++] = f.getName();
            } else if (OpenableColumns.SIZE.equals(col)) {
                values[i++] = f.getLength();
            }
        }
        values = FileProvider.copyOf(values, i);
        final MatrixCursor cursor = new MatrixCursor(projection, 1);
        cursor.addRow(values);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        if (isStorageUri(uri))
            return super.getType(uri);
        final TorrentPlayer.PlayerFile file = getPlayerFile(uri);
        if (file == null)
            return null;
        return TorrentPlayer.getType(file);
    }

    ParcelFileDescriptor openFile(final TorrentPlayer.PlayerFile file, String mode) throws FileNotFoundException {
        try {
            if (file.arch != null) {
                return openInputStream(new InputStreamWriter() {
                    @Override
                    public void copy(OutputStream os) throws IOException {
                        file.arch.copy(os);
                    }

                    @Override
                    public long getSize() {
                        return file.arch.getLength();
                    }

                    @Override
                    public void close() throws IOException {
                    }
                }, mode);
            } else {
                Uri uri = file.getFile();
                String s = uri.getScheme();
                if (s.equals(ContentResolver.SCHEME_CONTENT)) {
                    return resolver.openFileDescriptor(uri, mode);
                } else if (s.equals(ContentResolver.SCHEME_FILE)) {
                    File f = Storage.getFile(uri);
                    return ParcelFileDescriptor.open(f, FileProvider.modeToMode(mode));
                } else {
                    throw new Storage.UnknownUri();
                }
            }
        } catch (IOException e) {
            throw fnfe(e);
        }
    }

    @Nullable
    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        if (isStorageUri(uri))
            return super.openFile(uri, mode);
        final TorrentPlayer.PlayerFile file = getPlayerFile(uri);
        if (file == null)
            return null;
        return openFile(file, mode);
    }

    @Nullable
    @Override
    public AssetFileDescriptor openAssetFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        if (isStorageUri(uri))
            return super.openAssetFile(uri, mode);
        final TorrentPlayer.PlayerFile file = getPlayerFile(uri);
        if (file == null)
            return null;
        return new AssetFileDescriptor(openFile(file, mode), 0, AssetFileDescriptor.UNKNOWN_LENGTH);
    }
}
